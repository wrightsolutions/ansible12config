#!/bin/dash
# First argument is remote user
# Second (optional) argument is the hosts list
# Ansible itself will probably use /etc/ansible/hosts if you
# do not provide a second argument.
# No arguments given, then remote user will default to root
HOSTS_LIST_=''
REMOTE_USER_='root'
if [ $# -gt 0 ]; then
    if [ ! -z "$1" ]; then
        REMOTE_USER_=${1}
	if [ $# -gt 1 ]; then
	    if [ ! -z "$2" ]; then
		HOSTS_LIST_=${2}
	    fi
	fi
    fi
fi
if [ 'xxxx' = "xx${HOSTS_LIST_}xx" ]; then
    # The /sbin/ is necessary for ifconfig, but other
    # more common commands such as hostname or date
    # need not be specified with full path.
    ansible all -u ${REMOTE_USER_} -a /sbin/ifconfig
else
    ansible -i ${HOSTS_LIST_} all -u ${REMOTE_USER_} -a /sbin/ifconfig
fi
